import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomVeredaComponent } from './custom-vereda.component';

describe('CustomVeredaComponent', () => {
  let component: CustomVeredaComponent;
  let fixture: ComponentFixture<CustomVeredaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomVeredaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomVeredaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
