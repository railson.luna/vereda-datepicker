import { Component, OnInit } from '@angular/core';
import { Moment } from 'moment';
import * as moment from 'moment';

import * as localization from 'moment/locale/pt';
import { LocaleConfig } from './../../../../src/daterangepicker/daterangepicker.config';
moment.locale('pt', localization);


@Component({
  selector: 'custom-vereda',
  templateUrl: './custom-vereda.component.html',
  styleUrls: ['./custom-vereda.component.scss']
})
export class CustomVeredaComponent implements OnInit {

  public readonly locale: LocaleConfig = {
    daysOfWeek: moment.weekdaysShort(),
    monthNames: moment.months(),
    firstDay: moment.localeData().firstDayOfWeek(),
  }

  public selected: any;
  public alwaysShowCalendars: boolean;

  public readonly ranges: any = {
    'Limpar Filtro': [null],
    'Última Semana': [moment().subtract(6, 'days'), moment()],
    'Último Mês': [moment().subtract(29, 'days'), moment()],
    'Próxima Semana': [moment(), moment().add(6, 'days')],
  }
  public invalidDates: moment.Moment[] = [
    moment().add(2, 'days'),
    moment().add(3, 'days'),
    moment().add(5, 'days')
  ];

  isInvalidDate = (m: moment.Moment) =>  {
    return this.invalidDates.some(d => d.isSame(m, 'day') )
  }

  constructor() { }

  ngOnInit() {
  }

  public rangeClicked ($event): void {
    const {dates: [date]} = $event;
    if (!date.isValid()) {
      setTimeout(() => {
        this.selected = '';
      })
    }

  }


  public clear(): void {
    this.selected = '';
  }

}
